package edu.rit.cs.chord;

import java.io.File;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class Node extends UnicastRemoteObject implements RemoteNode {

    /**
     * All attributes used by Node
     *
     * nodeID - unique ID of this node
     * numNodes - the total number of nodes in this distributed system
     * activeNodes - a list of all the online nodes including this one
     * fingerTable - the finger table object for this node
     * client - a reference to the client used to update its list of active nodes
     * remotePath - the root path of the remote invocation registry we use
     * keys - a list of all the keys this node is responsible for storing
     * files - all of the files currently stored at this node
     */
    private int nodeID;
    private int numNodes;
    private List<RemoteNode> activeNodes;
    private FingerTable fingerTable;
    private RemoteClient client;
    private String remotePath;
    private List<Integer> keys;
    private List<File> files;

    /**
     * The constructor for this Node class, sets up all attributes
     * @param id - the ID for this node
     * @param n - the number of nodes in this system
     * @param remotePathName - the root path of the remote invocation registry we use
     * @param portNumber - the port number to host the registry on
     * @param scanner - the Scanner for system input
     * @throws RemoteException - thrown if any issues with the remote invocation
     */
    private Node(int id, int n, String remotePathName, int portNumber, Scanner scanner) throws RemoteException {
        super();

        nodeID = id;
        numNodes = n;
        this.remotePath = remotePathName;

        // Try to bind the name to the registry
        // If RemoteException is thrown, registry hasn't been created yet, so
        // create it and then try to bind again.
        // If RemoteException is still thrown, will be caught in main
        try {
            bindName(nodeID);
        } catch (RemoteException e) {
            LocateRegistry.createRegistry(portNumber);
            bindName(nodeID);
        }

        // Instantiate fileNames
        files = new ArrayList<>();

        // Iterate through all possible nodes and if they're active,
        // add them to the list and update all other nodes
        activeNodes = new ArrayList<>();

        for(int i = 0; i < numNodes; i++) {

            try {
                // Try to lookup each node in the system
                RemoteNode activeNode = (RemoteNode)Naming.lookup(remotePath + "/node" + i);

                // If reached this spot, activeNode was successfully looked up so add to list
                activeNodes.add(activeNode);

                // Add this node to all other nodes and update their information
                if(activeNode.getNodeID() != nodeID)
                    activeNode.addNodeToSystem(this);

            } catch (NotBoundException e) {
                //don't add to list, activeNode failed lookup, so it's not online
            } catch (MalformedURLException e) {
                e.printStackTrace();
                System.exit(1);
            }

        }

        // Create and update key list
        updateKeys();

        // Set up and update client
        try {
            client = (RemoteClient)Naming.lookup(remotePath + "/client");
            client.addNewNode(this);
        } catch (NotBoundException | MalformedURLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        // Initialize fingerTable
        fingerTable = new FingerTable(activeNodes, numNodes, this.getNodeID());

        // Redistribute files for all nodes
        for(RemoteNode node : activeNodes)
            node.redistributeFiles();

        // Hand off control to this loop
        inputLoop(scanner);

    }

    /**
     * Gets the ID of this node
     * @return nodeID
     */
    @Override
    public int getNodeID() {
        return this.nodeID;
    }

    /**
     * Adds a new node to this node's activeNodes list and updates its finger table
     * @param n - the new Node to add
     */
    @Override
    public void addNodeToSystem(RemoteNode n) throws RemoteException {
        activeNodes.add(n);
        fingerTable.updateSuccessors(activeNodes);
        updateKeys();
    }

    /**
     * Removes the given node and updates finger table
     * @param n - the node to remove
     */
    @Override
    public void removeNodeFromSystem(RemoteNode n) throws RemoteException{
        activeNodes.remove(n);
        fingerTable.updateSuccessors(activeNodes);
        updateKeys();
    }

    /**
     * Computes where this file would be stored at inside the distributed node system
     * @param file - the file in question
     * @return the id of the node that this file should be stored at
     */
    @Override
    public int determineStorageNode(File file) throws RemoteException{
        int key = (file.getName().hashCode() % numNodes + numNodes) % numNodes;
        return fingerTable.lookup(activeNodes, key, nodeID, new ArrayList<>());
    }

    /**
     * Adds given file to internal storage list of files
     * @param file - file to store
     */
    @Override
    public void addFileToList(File file) {
        files.add(file);
    }

    /**
     * Getter function for keys
     * @return the list of keys this node stores
     */
    @Override
    public List<Integer> getKeys() {
        return keys;
    }

    /**
     * Gets the node with nodeID id
     * @param id - id of node we are searching for
     * @return the node we are searching for
     * @throws RemoteException - if an issue with the remote invocation
     */
    @Override
    public RemoteNode getNodeWithID(int id) throws RemoteException {
        for(RemoteNode n : activeNodes) {
            if (n.getNodeID() == id)
                return n;
        }
        return null;
    }

    /**
     * Getter function for fingerTable
     * @return the finger table of this node
     */
    @Override
    public FingerTable getFingerTable() {
        return fingerTable;
    }

    /**
     * Attempts to find and return the file passed in files list
     * @param file - the file we are searching for
     * @return the file stored in files if it exists, null otherwise
     */
    @Override
    public File getFile(File file) {
        for(File f : files) {
            if (f.equals(file))
                return f;
        }
        return null;
    }

    /**
     * Redistributes all the files based on the current list of active nodes
     * Synchronized so only one node can do this at a time
     */
    @Override
    public synchronized void redistributeFiles() throws RemoteException {

        int originalFileSize = files.size();

        // For each file, determine where it should now go and send it there, then remove from this list
        for(int i = 0; i < originalFileSize; i++) {
            File file = files.get(i);

            int nodeIDToSendTo = activeNodes.get(0).determineStorageNode(file);

            RemoteNode node = getNodeWithID(nodeIDToSendTo);

            if(node != null) {
                node.addFileToList(file);
                files.remove(file);

                // Reset indices and file size to prevent concurrent modification
                i--;
                originalFileSize--;
            } else {
                System.err.println("Something went horribly wrong searching for Node " + nodeIDToSendTo);
                System.exit(1);
            }

        }
    }

    /**
     * Updates the list of keys that this node should store
     */
    private void updateKeys() throws RemoteException {
        // Boolean value used to help in wrap around functionality of searching through nodes
        boolean reachedZero = false;

        // Initialize a new list to start from scratch and add this node's key
        keys = new ArrayList<>();
        keys.add(nodeID);

        // Start checking from the next smaller node
        for(int i = nodeID - 1; i >= 0; i--) {
            if(activeNodes.contains(getNodeWithID(i))) {
                break;
            } else {
                keys.add(i);
                if(i == 0)
                    reachedZero = true;
            }
        }

        // If we have to wrap back around to the node with the largest ID number, do so and continue loop
        for(int i = numNodes - 1; (reachedZero || nodeID == 0) && i > nodeID; i--) {
            if(activeNodes.contains(getNodeWithID(i))) {
                break;
            } else {
                keys.add(i);
            }
        }
    }

    /**
     * Attempts to bind a String to the registry
     * @param id - the id of the object we are trying to bind
     * @throws RemoteException if the registry doesn't exist or some other remote error occurred
     */
    private void bindName(int id) throws RemoteException {
        try {
            Naming.rebind(remotePath + "/node" + id, this);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Removes the node with ID passed from activeNodes
     * @param id - the ID of the node to remove
     * @throws RemoteException - if there was an issue
     */
    private void removeNodeIDFromList(int id) throws RemoteException {
        int indexToRemove = -1;

        // Find index of node with ID id
        for(int i = 0; i < activeNodes.size(); i++) {
            if(activeNodes.get(i).getNodeID() == id) {
                indexToRemove = i;
                break;
            }
        }

        if(indexToRemove < 0) {
            System.err.println("Something went horribly wrong trying to remove node " + id);
        } else {
            activeNodes.remove(indexToRemove);
        }

    }

    /**
     * Performs a clean logout of this node with updates in all other nodes
     * and redistributing any files this node stores
     * @throws RemoteException - if an issue with the remote invocation
     */
    private void logout() throws RemoteException {
        // Remove this node from all other nodes' list of active nodes
        for(RemoteNode n : activeNodes) {
            if(n.getNodeID() != nodeID)
                n.removeNodeFromSystem(this);
        }

        // Remove this node from its own list to distribute its files correctly
        removeNodeIDFromList(this.nodeID);

        // Redistribute the files of the node logging off (unless it's the last one in the system)
        if(activeNodes.size() > 0)
            redistributeFiles();

        // Redistribute the rest of the files with their updated finger tables
        for(RemoteNode node : activeNodes)
            node.redistributeFiles();

        // Remove node from client's list of active nodes
        client.removeNode(this);

        try {
            Naming.unbind(remotePath + "/node" + nodeID);
        } catch (NotBoundException | MalformedURLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        System.out.println("Logging out...");
        System.exit(0);
    }

    /**
     * Prints out a nice looking view of the commands
     */
    private void printCommands() {
        System.out.println("******** COMMANDS *********");
        System.out.println("*  0. Print all commands  *");
        System.out.println("*  1. Print out node info *");
        System.out.println("*  2. Logout              *");
        System.out.println("***************************");
        System.out.println();
    }

    /**
     * The main input loop for this node to allow the node to see display certain attributes of itself if requested
     * @param sc - Scanner for input, will probably not need this anymore
     * @throws RemoteException - thrown if any issues with remote invocation
     */
    private void inputLoop(Scanner sc) throws RemoteException {

        //Clear buffer from previous integer input
        sc.nextLine();

        // Set up information for this function
        String enterInput = "Enter command (\"0\" for list of commands): ";

        // Initial display and input
        printCommands();
        System.out.print(enterInput);
        int input = Integer.parseInt(sc.nextLine());

        // Input loop itself executing appropriate functionality for each command
        while(true) {
            switch (input) {
                case 0:
                    printCommands();
                    break;
                case 1:
                    System.out.println(this);
                    break;
                case 2:
                    logout();
                default:
                    System.out.println("Invalid command please try again.");
            }

            // Get next input
            System.out.print(enterInput);
            input = Integer.parseInt(sc.nextLine());
        }

    }

    /**
     * String containing all information stored in node that we wish to see
     *  - node ID
     *  - number of nodes in system
     *  - active nodes in system
     *  - keys stored at this node
     *  - finger table of this node
     *  - files stored at this node
     * @return nicely formatted string with node's information
     */
    @Override
    public String toString() {
        // Initial information
        String result = "\nNode " + nodeID + "'s information:\n";
        result += "Number of nodes in system: " + numNodes + "\n\n";

        // Active nodes
        result += "Actives nodes in system: ";
        for(RemoteNode node : activeNodes) {
            try {
                result += node.getNodeID() + " ";
            } catch (RemoteException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        result += "\n";

        // Keys
        result += "Keys: ";
        for(Integer k : keys) {
            result += k + " ";
        }
        result += "\n\n";

        // Finger Table
        result += fingerTable;

        // Files
        result += "\nFiles currently stored:\n";

        if(files.size() == 0) {
            result += "\tNone\n";
        } else {
            for (File f : files) {
                result += "\t" + f.getName() + "\n";
            }
        }

        result += "\n";

        return result;
    }

    /**
     * Main function that gets initial information and passes off control to the constructor
     * @param args - program arguments, args[0] should contain number of nodes (16), args[1] should contain host name
     *             and args[2] should contain port number
     */
    public static void main(String[] args) throws RemoteException {

        // Validate program arguments
        if(args.length < 3) {
            System.err.println("Usage error:");
            System.err.println("Program Argument 1: number of nodes for system.");
            System.err.println("Program Argument 2: host name for remote invocation");
            System.err.println("Program Argument 3: port name for remote invocation");
            System.exit(1);
        }

        // Store program arguments
        int numberOfNodes = Integer.parseInt(args[0]);
        String hostName = args[1];
        int portNumber = Integer.parseInt(args[2]);

        // Set up scanner and loop boolean value
        Scanner sc = new Scanner(System.in);
        boolean validID = false;

        while(!validID) {
            // Ask for input
            System.out.print("What is your ID: ");
            int id = sc.nextInt();

            // Check if ID is available, if not, loop again
            // If available, go ahead and construct it
            String path = "rmi://" + hostName + ":" + portNumber;

            try {
                Naming.lookup(path + "/node" + id);
                // If we reached this point, that means the lookup was successful
                // so ID is already taken
                System.out.println("ID is already taken, please try again.");
            } catch(RemoteException | NotBoundException e) {
                new Node(id, numberOfNodes, path, portNumber, sc);
                validID = true;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
}
