package edu.rit.cs.chord;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RemoteNode extends Remote {

    /**
     * Gets the ID of this node
     * @return nodeID
     */
    int getNodeID() throws RemoteException;

    /**
     * Adds a new node to this node's activeNodes list and updates its finger table
     * @param n - the new Node to add
     */
    void addNodeToSystem(RemoteNode n) throws RemoteException;

    /**
     * Removes the given node and updates finger table
     * @param n - the node to remove
     */
    void removeNodeFromSystem(RemoteNode n) throws RemoteException;

    /**
     * Computes where this file would be stored at inside the distributed node system
     * @param file - the file in question
     * @return the id of the node that this file should be stored at
     */
    int determineStorageNode(File file) throws RemoteException;

    /**
     * Adds given file to internal storage list of files
     * @param file - file to store
     */
    void addFileToList(File file) throws RemoteException;

    /**
     * Getter function for keys
     * @return the list of keys this node stores
     */
    List<Integer> getKeys() throws RemoteException;

    /**
     * Gets the node with nodeID id
     * @param id - id of node we are searching for
     * @return the node we are searching for
     * @throws RemoteException - if an issue with the remote invocation
     */
    RemoteNode getNodeWithID(int id) throws RemoteException;

    /**
     * Getter function for fingerTable
     * @return the finger table of this node
     */
    FingerTable getFingerTable() throws RemoteException;

    /**
     * Attempts to find and return the file passed in files list
     * @param file - the file we are searching for
     * @return the file stored in files if it exists, null otherwise
     */
    File getFile(File file) throws RemoteException;

    /**
     * Redistributes all the files based on the current list of active nodes
     * Synchronized so only one node can do this at a time
     */
    void redistributeFiles() throws RemoteException;

}
