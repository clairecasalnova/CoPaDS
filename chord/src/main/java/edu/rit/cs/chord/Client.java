package edu.rit.cs.chord;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client extends UnicastRemoteObject implements RemoteClient {

    /**
     * Attributes used by Client
     */
    private List<RemoteNode> activeNodes;
    private String fileRootPath;

    /**
     * Constructor for Client creating the registry, binding itself as a remote object,
     * instantiating attributes and handing off control to inputLoop
     *
     * @param remotePath - the root path of the remote invocation registry we use
     * @param portNumber - the port number to host the registry on
     * @throws RemoteException - if an issue with remote invocation
     */
    private Client(String remotePath, int portNumber) throws RemoteException {

        LocateRegistry.createRegistry(portNumber);

        try {
            Naming.rebind(remotePath + "/client", this);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        activeNodes = new ArrayList<>();
        fileRootPath = "../CoPaDS/chord/src/main/java/edu/rit/cs/chord/files/";

        inputLoop();
    }

    /**
     * Adds the given RemoteNode to activeNodes
     * @param n - the node to add
     */
    @Override
    public void addNewNode(RemoteNode n) {
        activeNodes.add(n);
    }

    /**
     * Removes the given RemoteNode from activeNodes
     * @param n - the node to remove
     */
    @Override
    public void removeNode(RemoteNode n) {
        activeNodes.remove(n);
    }

    /**
     * Main function to lookup a file as the client
     * @param file - the file we are searching for
     * @param nodeID - the ID of the node we wish to search from
     * @return the ID of the node that should be storing this file
     * @throws RemoteException - if issue
     */
    private int queryFile(File file, int nodeID) throws RemoteException {

        // Get the node we want to start the lookup from
        RemoteNode node = getNodeFromList(nodeID);
        int result;

        if(node != null) {
            // Hand off control the the nodes to find the correct one
            result = node.determineStorageNode(file);
        } else {
            // Desired node wasn't online, search from first online one
            System.out.println("Node " + nodeID + " is not online, searching from a different node.");
            result = activeNodes.get(0).determineStorageNode(file);
        }

        return result;
    }

    /**
     * Main function to store a file as the client
     * @param file - the file we wish to store
     * @throws RemoteException - if issue
     */
    private void storeFile(File file) throws RemoteException {

        if(activeNodes.isEmpty()) {
            System.out.println("No nodes are online to store your file.");
        } else {

            // Find where it should be stored, doesn't matter which node we start from to do this
            int id = activeNodes.get(0).determineStorageNode(file);
            RemoteNode node = getNodeFromList(id);

            // Store the file at this location
            if(node != null) {
                node.addFileToList(file);
                System.out.println("File stored successfully.");
            } else {
                System.err.println("Error trying to store file at node " + id + ".");
                System.exit(1);
            }
        }

    }

    /**
     * Searches the list of active nodes to find the one with the desired ID
     * @param id - ID of node we wish to get
     * @return the desired node or null if it's not online
     * @throws RemoteException - if issue
     */
    private RemoteNode getNodeFromList(int id) throws RemoteException {

        for(RemoteNode n : activeNodes) {
            if(n.getNodeID() == id)
                return n;
        }

        return null;
    }

    /**
     * Prints out a nice looking view of the commands
     */
    private void printCommands() {
        System.out.println("******** COMMANDS *********");
        System.out.println("*  0. Print all commands  *");
        System.out.println("*  1. Store a file        *");
        System.out.println("*  2. Lookup a file       *");
        System.out.println("*  3. Logout              *");
        System.out.println("***************************");
        System.out.println();
    }

    /**
     * Helper function to storeFile that takes care of input
     * and just passes the desired file to storeFile
     * @param sc - Scanner for input
     */
    private void storeFileHelper(Scanner sc) {

        // Get file
        File fileToStore = readInFileFromInput(sc, true);

        // Test to make sure file exists within our project
        try {
            new FileReader(fileToStore);
            storeFile(fileToStore);
        } catch (FileNotFoundException e) {
            System.out.println("Unable to find the file, please try again.");
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        System.out.println();
    }

    /**
     * Helper function to queryFile that takes care of input and passes the
     * desired file and node to queryFile
     * @param sc - Scanner for input
     */
    private void queryFileHelper(Scanner sc) {

        // Get file
        File fileToSearch = readInFileFromInput(sc, false);

        // Ask which node
        System.out.print("Which node would you like to search from (enter ID): ");
        int nodeIDToSearch = Integer.parseInt(sc.nextLine());

        try {
            // Find ID of node this file should be stored at
            int nodeFound = queryFile(fileToSearch, nodeIDToSearch);

            if(nodeFound >= 0) {

                // Get the node the file should be stored at
                RemoteNode node = getNodeFromList(nodeFound);

                if(node != null) {
                    // Get the file from the node and display its contents
                    File fileFound = node.getFile(fileToSearch);
                    displayFile(fileFound);
                } else {
                    System.err.println("Something went horribly wrong trying to find node " + nodeFound);
                    System.exit(1);
                }

            } else {
                System.err.println("Something went horribly wrong querying for this file.");
                System.exit(1);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper function to ask user what file they want to work with
     * @param sc - the Scanner
     * @return the File object with the name the user inputted
     */
    private File readInFileFromInput(Scanner sc, boolean storing) {

        if(storing)
            System.out.println("\nNote: The file you wish to store must be located " +
                "within \"files\" package inside edu.rit.cs.chord!");

        System.out.print("Enter the name of the file: ");
        String fileName = sc.nextLine();

        return new File(fileRootPath + fileName);
    }

    /**
     * Displays the contents of the file given just as it appears in the file
     * @param file - the file whose contents we wish to display
     */
    private void displayFile(File file) {

        if(file != null) {
            System.out.println("File was found. Here are its contents:\n");

            try {
                FileReader fr = new FileReader(file);

                // Print out the file
                int i;
                while((i = fr.read()) != -1)
                    System.out.print((char) i);
                System.out.println("\n");
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }

        } else {
            System.out.println("File was not found in the system.");
        }
    }

    /**
     * Main input loop taking taking in the command inputs and executing them appropriately
     */
    private void inputLoop() {

        // Set up information for this function
        Scanner sc = new Scanner(System.in);
        String enterInput = "Enter command (\"0\" for list of commands): ";

        // Initial display and input
        printCommands();
        System.out.print(enterInput);
        int input = Integer.parseInt(sc.nextLine());

        // Input loop itself executing appropriate functionality for each command
        while(input != 3) {
            switch (input) {
                case 0:
                    printCommands();
                    break;
                case 1:
                    if(activeNodes.size() > 0)
                        storeFileHelper(sc);
                    else
                        System.out.println("No nodes are online, please start at least one.");
                    break;
                case 2:
                    if(activeNodes.size() > 0)
                        queryFileHelper(sc);
                    else
                        System.out.println("No nodes are online, please start at least one.");
                    break;
                default:
                    System.out.println("Invalid command please try again.");
            }

            // Get next input
            System.out.print(enterInput);
            input = Integer.parseInt(sc.nextLine());
        }

        System.out.println("Logging out...");
        System.exit(0);
    }

    /**
     * Main function that gets initial information and hands off control to constructor
     * @param args - program arguments, arg[0] should contain host name and args[1] port number
     */
    public static void main(String[] args) {

        // Validate program arguments
        if(args.length < 2) {
            System.err.println("Usage error:");
            System.err.println("Program Argument 1: host name for remote invocation");
            System.err.println("Program Argument 2: port name for remote invocation");
            System.exit(1);
        }

        // Store program arguments
        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        // Build RMI path and Scanner
        String path = "rmi://" + hostName + ":" + portNumber;

        try {
            new Client(path, portNumber);
        } catch (RemoteException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }
}
