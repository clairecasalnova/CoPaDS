package edu.rit.cs.chord;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteClient extends Remote {

    /**
     * Adds the given RemoteNode to activeNodes
     * @param n - the node to add
     */
    void addNewNode(RemoteNode n) throws RemoteException;

    /**
     * Removes the given RemoteNode from activeNodes
     * @param n - the node to remove
     */
    void removeNode(RemoteNode n) throws RemoteException;
}
