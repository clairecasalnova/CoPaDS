package edu.rit.cs.chord;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;


public class FingerTable implements Serializable{

    /**
     * All attributes used by FingerTable
     *
     * table -- a 2d array that forms the entries of the finger table
     * ID-- the ID of the node that the finger table corresponds to
     */
    private int[][] table;
    private int ID;

    /**
     * Constructor for FingerTable
     * creates the finger table based on the nodes that are online
     * sets the ID variable to that of the node that this finger table corresponds with
     * @param activeNodes - list of all active nodes
     * @param n - number of total nodes for the system
     */
    public FingerTable(List<RemoteNode> activeNodes, int n, int id) throws RemoteException {
        this.ID = id;
        int rows = log2(n);
        table = new int[rows][2];
        for (int i = 0; i < rows; i++){
            table[i][0] = jFunc(id, i, n);
        }
        updateSuccessors(activeNodes);
    }

    /**
     *
     * @param nodes sorts the nodes in the activeNodes array
     * @throws RemoteException
     */
    private void quicksort(List<RemoteNode> nodes) throws RemoteException {
        quicksortHelp(nodes, 0, nodes.size() - 1);
    }

    /**
     * helper function for quicksort
     * @param nodes the list of nodes to be sorted
     * @param l left index of the array
     * @param h size of the array
     * @throws RemoteException
     */
    private void quicksortHelp(List<RemoteNode> nodes, int l, int h) throws RemoteException {
        while(l < h) {
            int p = partition(nodes, l, h);

            //if the left side is smaller than the right, recursively sort left and iteratively sort right
            if(p - l < h - p) {
                quicksortHelp(nodes, l, p - 1);
                l = p + 1;
            } else {
                quicksortHelp(nodes, p + 1, h);
                h = p - 1;
            }
        }
    }

    /**
     * partition the list into different sections to be sorted
     * @param nodes the list of nodes that is being sorted
     * @param l left index of the list
     * @param h right side of the list
     * @return return the index of the partition
     * @throws RemoteException
     */
    private static int partition(List<RemoteNode> nodes, int l, int h) throws RemoteException {
        int x = nodes.get(h).getNodeID();
        int i = l - 1;
        int j = l;
        while(j < h) {
            if(nodes.get(j).getNodeID() <= x) {
                i++;
                swap(nodes, i, j);
            }
            j++;
        }
        swap(nodes, i + 1, h);
        return i + 1;
    }

    /**
     * function to swap the positions when sorting
     * @param nodes  the list of nodes being sorted
     * @param i the index of one of the nodes
     * @param j index of the node being swapped in
     */
    private static void swap(List<RemoteNode> nodes, int i, int j) {
        RemoteNode temp = nodes.get(i);
        nodes.set(i, nodes.get(j));
        nodes.set(j, temp);
    }

    /**
     * calculate the log base 2
     * used to find the number of rows in the finger table based on the number of nodes
     * @param x the number that the log is being take of
     * @return log base 2 of x
     */
    private static int log2(int x) {
       return (int) (Math.log(x)/Math.log(2));
    }

    /**
     * calculates j + 2 ^i
     * used to calculate the intended successor for the node
     * @param j the j value which is the node ID
     * @param i the i value which is the row value of the table
     * @param n the number of nodes in the system
     * @return the value of the intended successor
     */
    private static int jFunc(int j, int i, int n) {
        int val = (int) (j + Math.pow(2, i));
        val = val % n;
        return val;
    }

    /**
     * updates the successors whenever a new node comes online
     * the successors are the nodes that are online that are closest to the intended successors
     * @param activeNodes the list of nodes that are online
     * @throws RemoteException
     */
    public void updateSuccessors(List<RemoteNode> activeNodes) throws RemoteException {

        quicksort(activeNodes);

        for (int i = 0; i < table.length; i++) {
            int jVal = table[i][0];
            boolean placed = false;
            for (int j = 0; j < activeNodes.size(); j++) {
                int ID = activeNodes.get(j).getNodeID();

                if (placed) {
                    break;
                }

                if (ID == jVal) {
                    table[i][1] = jVal;
                    placed = true;
                } else if (ID > jVal) {
                    table[i][1] = ID;
                    placed = true;
                }
            }
            if (!placed) {
                table[i][1] = activeNodes.get(0).getNodeID();
            }
        }
     }

    /**
     * function to test the algorithm written in update successors
     */
    public void test() {
        int[] t = {1,2,8,14};

        for (int i = 0; i < table.length; i++) {
            int jVal = table[i][0];
            boolean placed = false;

            for (int j = 0; j < t.length; j++) {
                int ID = t[j];

                if (placed) {
                    break;
                } else if (ID == jVal) {
                    table[i][1] = jVal;
                    placed = true;

                } else if (ID > jVal) {
                    table[i][1] = ID;
                    placed = true;
                }

            }
            if (!placed) {
                table[i][1] = t[0];
            }
        }

        for (int i = 0; i < table.length; i++) {
            for (int j = 0;j < table[0].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }

    }

    /**
     * lookup function to find where the given key is stored within the table
     * @param activeNodes a list of active nodes in the system
     * @param key the node id that we are attempting to find
     * @param nodeID the id of the node that is looking at its finger table
     * @param visitedNodes a list of the nodes that have already looked at their finger table for the key
     * @return the ID of the node that has the key
     * @throws RemoteException
     */
    public int lookup(List<RemoteNode> activeNodes, int key, int nodeID,List<Integer>visitedNodes) throws RemoteException{
        int successor = -1;
        if (visitedNodes.contains(nodeID)) {
            RemoteNode n = activeNodes.get(0).getNodeWithID(nodeID);
            for (int k : n.getKeys()) {
                if (k == key) {
                    return nodeID;
                }
            }
        }

        visitedNodes.add(nodeID);
        int max = -1;
        int index = 0;
        for (int i = 0; i < table.length; i++) {
            int jVal = table[i][0];
            if (jVal > max && jVal <= key) {
                max = jVal;
                index = i;
            }
        }
        successor = table[index][1];
        if (successor == key) {
            return successor;
        } else {
            return activeNodes.get(0).getNodeWithID(successor).getFingerTable().
                    lookup(activeNodes, key, successor, visitedNodes);
        }
    }

    /**
     * creates a string representation of the table
     * @return the string that shows the table of values
     */
    @Override
    public String toString() {
        String s = "%-8s %-10s %-10s \n";
        String result = "Finger Table for Node #" + ID+"\n";
        result += String.format(s,"i",  "j + 2^i",  "successor");
        result += "------------------------------\n";
        for (int i = 0; i < table.length; i++){
            result += String.format(s ,Integer.toString(i), Integer.toString(table[i][0]),  Integer.toString(table[i][1]));
        }
        return result;
    }
}
