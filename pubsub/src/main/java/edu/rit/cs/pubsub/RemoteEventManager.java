package edu.rit.cs.pubsub;

import java.net.MalformedURLException;
import java.rmi.*;
import java.util.List;

public interface RemoteEventManager extends Remote {

    /* All functions that you want that PubSubAgent can call on EventManager */

    /**
     * Adds s to subscriber list for specified topic in topicSubscribers map
     * @param topic - topic to which s wants to subscribe
     * @param s - the subscriber making the request
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void addToTopic(Topic topic, Subscriber s) throws RemoteException;

    /**
     * Adds s to subscriber list for topics containing keyword in topicSubscribers map
     * @param keyword - keyword of topics to which s wants to subscribe
     * @param s - the subscriber making the request
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void addToTopics(String keyword, Subscriber s) throws RemoteException;

    /**
     * Removes s from subscriber list for specified topic in topicSubscribers map
     * @param topic - topic from which s wants to unsubscribe
     * @param s - the subscriber making the request
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void removeFromTopic(Topic topic, Subscriber s) throws RemoteException;

    /**
     * Removes s from subscriber list for all topics it is currently subscribed to
     * @param s - the subscriber making the request
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void removeFromTopics(Subscriber s) throws RemoteException;

    /**
     * A request made by the subscriber to see all topics it is currently subscribed to
     * @param s - the subscriber making the request
     * @return a formatted String containing all topics s is subscribed to, returns special message if none
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    String subscriberTopics(Subscriber s) throws RemoteException;

    /**
     * A publish request was made by a publisher, so we must notify all subscribers of
     * the event's topic as well as store the event in our cache if a subscriber is not online
     * @param event - event which a publisher wishes to publish
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void notifySubscribers(Event event) throws RemoteException;

    /**
     * An advertise request was made by a publisher, so we must notify all subscribers
     * of the new topic
     * @param topic - topic which a publisher wishes to advertise
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void addTopic(Topic topic) throws RemoteException;

    /**
     * A subscriber (new or existing) has logged in and must be added to our internal list
     * of onlineSubscribers as well as allSubscribers (if new)
     * @param s - the subscriber logging in
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void addSubscriber(Subscriber s) throws RemoteException, NotBoundException, MalformedURLException;

    /**
     * A subscriber is logging off, so remove them from internal list of onlineSubscribers
     * @param s - the subscriber logging off
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    void removeSubscriber(Subscriber s) throws RemoteException, NotBoundException, MalformedURLException;

    /**
     * A request has been made to see all the existing topics
     * @return a formatted String containing all existing topics
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    List<Topic> listTopics() throws RemoteException;

    /**
     * A subscriber has just logged in and is checking to see if it missed any messages, send them
     * all missed messages then remove them from the cache
     * @param s - the subscriber making the request
     * @return a formatted String containing all messages s missed while offline, returns special message if none
     * @throws RemoteException - thrown if there is an error in remote connection
     */
    String checkCache(Subscriber s) throws RemoteException;

    /**
     *  determines if the subscriber that is currently logging in has already connected to the system
     * @param s - subscriber trying to log in
     * @return true if the agent has logged in before, false otherwise
     * @throws RemoteException  - thrown if there is an error in remote connection
     */
    boolean inSubscribers(Subscriber s) throws RemoteException;

}
