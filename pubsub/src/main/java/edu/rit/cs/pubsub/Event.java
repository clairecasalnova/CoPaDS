package edu.rit.cs.pubsub;
import java.io.Serializable;

public class Event implements Serializable {
    /**
     * Used to keep track of the number of events so an appropriate and unique ID can be assigned
     */
    private static int numEvents = 0;

    /**
     * Attributes of the Event object
     */
    private int id;
    private Topic topic;
    private String title;
    private String content;

    /**
     * Constructor for the Event object
     * @param t - the topic this event is linked to
     * @param title - title of the event
     * @param content - text content of the event
     */
    public Event(Topic t, String title, String content){
        this.id = numEvents++;
        this.topic = t;
        this.title = title;
        this.content = content;
    }

    /**
     * @return the topic this event is linked to
     */
    Topic getTopic() {
        return topic;
    }

    /**
     * @return a formatted string describing the event and its contents
     */
    @Override
    public String toString() {
        String eventStr = "Event " + id + ":\n";
        eventStr += "\tTopic: " + topic.getName() + "\n";
        eventStr += "\tTitle: " + title + "\n";
        eventStr += "\tContent: " + content + "\n";
        return eventStr;
    }
}
