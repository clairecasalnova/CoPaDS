package edu.rit.cs.pubsub;

import java.rmi.RemoteException;

public interface Publisher {

	/**
	 * function called when a publisher wants to publish a new event to the subscribers
	 * calls the event manager to notify each of the subscribers
	 * @param event the new event to be published
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void publish(Event event) throws RemoteException;

	/**
	 * function called when a publisher wants to advertise a new topic to be subscribed to
	 * calls event manager to notify all of the subscribers of the new topic
	 * @param newTopic the new topic
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void advertise(Topic newTopic) throws RemoteException;
}
