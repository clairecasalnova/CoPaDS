package edu.rit.cs.pubsub;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;


public class EventManager extends UnicastRemoteObject implements RemoteEventManager {

	/**
	 * A list of all subscribers that have ever connected - uses their usernames
	 */
	private List<String> allSubscribers;

	/**
	 * A list of all online subscribers
	 */
	private List<Subscriber> onlineSubscribers;

	/**
	 * A map of topics to its list of subscribers (uses their usernames)
	 */
	private Map<Topic, List<String>> topicSubscribers;

	/**
	 * A map of subscribers (uses their usernames) to its cache of messages if they're offline
	 */
	private Map<String, List<String>> messageCache;

	/**
	 * Constructor for EventManager - sets up the object as a remote object then passes
	 * off execution to startService()
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	EventManager() throws RemoteException {
		super();

		//Instantiates all attributes;
		allSubscribers = new ArrayList<>();
		onlineSubscribers = new ArrayList<>();
		topicSubscribers = new HashMap<>();
		messageCache = new HashMap<>();

		//Creates an rmi registry within the server JVM with port number 8080
		LocateRegistry.createRegistry(8080);

		//Binds this EventManager object by the name eventManager and passes off execution
		try {
			Naming.rebind("rmi://localhost:8080/eventManager", this);

			startService();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Main function that immediately passes off execution to the constructor
	 * @param args - any arguments passed upon runtime, none expected
	 */
	public static void main(String[] args) {
		try {
			new EventManager();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * An input loop that allows user to either shut down the system
	 * or show a list of subscribers for a certain topic
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	private void startService() throws RemoteException {

		Scanner sc = new Scanner(System.in);
		String response;

		while(true) {
			System.out.println("MENU OF COMMANDS");
			System.out.println("\t1. List all subscribers of a certain topic.");
			System.out.println("\t2. List all existing topics and their subscribers.");
			System.out.println("\t3. List all online subscribers.");
			System.out.println("\t4. List all subscribers.");
			System.out.println("\t5. List all subscriber cached messages.");
			System.out.println("\t6. Send a message to all subscribers.");
			System.out.println("\t7. Exit system. (Warning: all data will be lost!)");
			System.out.print("Please enter the number of the action to perform: ");
			response = sc.nextLine();
			System.out.println();

			int responseInt = Integer.parseInt(response);

			switch(responseInt) {
				case 1:
					listSubscribersForTopic(sc);
					break;
				case 2:
					listAllTopicsAndSubscribers();
					break;
				case 3:
					listAllOnlineSubscribers();
					break;
				case 4:
					listAllSubscribers();
					break;
				case 5:
					listAllCachedMessages();
					break;
				case 6:
					sendMessage(sc);
					break;
				case 7:
					System.exit(0);
					break;
				default:
					System.out.println("Error: enter a valid command.");
					break;
			}
		}
	}

	/**
	 * A way to send a message inputted by the keyboard (mainly for debugging/testing purposes)
	 * @param sc - the Scanner for this process
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	private void sendMessage(Scanner sc) throws RemoteException {
		System.out.print("Enter the message you wish to send to all subscribers: ");
		String message = sc.nextLine();

		for(Subscriber s : onlineSubscribers)
			s.printMessage(message);

		System.out.println("Sent!");
	}

	/**
	 * Lists all the subscribers for a specific topic
	 * @param sc - the Scanner for this process
	 */
	private void listSubscribersForTopic(Scanner sc) {
		if(listTopics().isEmpty())
			System.out.println("There are no existing topics.\n");
		else {
			System.out.println("Here is a list of topics and their ID's:\n");
			for(Topic t : listTopics()) {
				System.out.println("Topic \"" + t.getName() + "\"");
			}

			System.out.print("Which topic would you like to see a list of subscribers for?\nEnter topic name: ");

			String response = sc.nextLine();
			Topic topic = returnTopic(response);

			System.out.println();

			if(topic != null) {
				showSubscribers(topic);
			} else {
				System.out.println("Invalid topic ID inputted, please try again.\n");
			}
		}

	}

	/**
	 * Lists all topics and their subscribers
	 */
	private void listAllTopicsAndSubscribers() {
		if(listTopics().isEmpty()) {
			System.out.println("There are no existing topics.\n");
		} else {
			for(Topic t : listTopics()) {
				showSubscribers(t);
			}
		}
	}

	/**
	 * Lists all online subscribers
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	private void listAllOnlineSubscribers() throws RemoteException {
		if(onlineSubscribers.isEmpty())
			System.out.println("There are no online subscribers.\n");
		else {
			System.out.println("All online subscribers:");
			for(Subscriber s : onlineSubscribers) {
				System.out.println("\t" + s.getUsername());
			}
			System.out.println();
		}
	}

	/**
	 * Lists all subscribers
	 */
	private void listAllSubscribers() {
		if(allSubscribers.isEmpty())
			System.out.println("No subscribers have logged in.\n");
		else {
			System.out.println("All subscribers:");
			for(String s : allSubscribers) {
				System.out.println("\t" + s);
			}
			System.out.println();
		}
	}

	/**
	 * Lists all subscribers that have cached messages as well as said messages
	 */
	private void listAllCachedMessages() {
		if(messageCache.keySet().isEmpty())
			System.out.println("There are no cached messages.\n");
		else {
			for(String s : messageCache.keySet()) {
				System.out.println(s + "'s messages:");
				for(String msg : messageCache.get(s)) {
					System.out.println("\t" + msg);
				}
			}
		}
	}

	/**
	 * Prints out all the subscribers for a certain topic
	 * @param topic - topic whose subscribers we wish to see
	 */
	private void showSubscribers(Topic topic) {
		if(topicSubscribers.get(topic).isEmpty())
			System.out.println("Topic \"" + topic.getName() + "\" has no subscribers.");
		else {
			System.out.println("All subscribers for Topic \"" + topic.getName() + "\":");

			for(String s : topicSubscribers.get(topic)) {
				System.out.println("\t" + s);
			}
		}
		System.out.println();
	}

	/**
	 * Adds s to subscriber list for specified topic in topicSubscribers map
	 * @param topic - topic to which s wants to subscribe
	 * @param s - the subscriber making the request
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	@Override
	public void addToTopic(Topic topic, Subscriber s) throws RemoteException {
		if(topicSubscribers.containsKey(topic)) {
			topicSubscribers.get(topic).add(s.getUsername());
		}
	}

	/**
	 * Adds s to subscriber list for topics containing keyword in topicSubscribers map
	 * @param keyword - keyword of topics to which s wants to subscribe
	 * @param s - the subscriber making the request
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	@Override
	public void addToTopics(String keyword, Subscriber s) throws RemoteException {
		for(Topic t : topicSubscribers.keySet()) {
			if(t.getKeywords().contains(keyword)) {
				addToTopic(t, s);
			}
		}
	}

	/**
	 * Removes s from subscriber list for specified topic in topicSubscribers map
	 * @param topic - topic from which s wants to unsubscribe
	 * @param s - the subscriber making the request
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	@Override
	public void removeFromTopic(Topic topic, Subscriber s) throws RemoteException {
		if(topicSubscribers.containsKey(topic)) {
			topicSubscribers.get(topic).remove(s.getUsername());
		}
	}

	/**
	 * Removes s from subscriber list for all topics it is currently subscribed to
	 * @param s - the subscriber making the request
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	@Override
	public void removeFromTopics(Subscriber s) throws RemoteException {
		for(Topic t : topicSubscribers.keySet()) {
			if(topicSubscribers.get(t).contains(s.getUsername()))
				removeFromTopic(t, s);
		}
	}

	/**
	 * A request made by the subscriber to see all topics it is currently subscribed to
	 * @param s - the subscriber making the request
	 * @return a formatted String containing all topics s is subscribed to, returns special message if none
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	@Override
	public String subscriberTopics(Subscriber s) throws RemoteException {
		String strTopics = "";

		for(Topic t : topicSubscribers.keySet()) {
			if(topicSubscribers.get(t).contains(s.getUsername())) {
				strTopics += t;
				strTopics += "\n";
			}
		}

		if(strTopics.equals(""))
			strTopics = "You are not subscribed to any topics.\n";
		else
			strTopics += "\n";

		return strTopics;
	}

	/**
	 * A publish request was made by a publisher, so we must notify all subscribers of
	 * the event's topic as well as store the event in our cache if a subscriber is not online
	 * @param event - event which a publisher wishes to publish
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	@Override
	public void notifySubscribers(Event event) throws RemoteException {
		Topic eventTopic = event.getTopic();
		List<String> subscribers = topicSubscribers.get(eventTopic);

		String msg = "A new event has been published under topic \"" + eventTopic.getName() + "\"!\n" + event;

		for(String s : subscribers) {
			Subscriber sub = getOnlineSubscriber(s);
			if(sub != null) {
				sub.printMessage(msg);
			} else {
				addToCache(s, msg);
			}
		}
	}

	/**
	 * Helper function that gets the online subscriber with name s
	 * @param s - name of subscriber searching for
	 * @return the subscriber with name s or null if not online
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	private Subscriber getOnlineSubscriber(String s) throws RemoteException {
		for(Subscriber sub : onlineSubscribers) {
			if(s.equals(sub.getUsername()))
				return sub;
		}
		return null;
	}

	/**
	 * Adds the given message to s's cache of messages, creates a new entry if first message in cache for s
	 * @param s - the subscriber in question
	 * @param message - the message to add to the cache
	 */
	private void addToCache(String s, String message) {
		if(!messageCache.containsKey(s)) {
			messageCache.put(s, new ArrayList<>());
		}

		messageCache.get(s).add(message);
	}


	/**
	 * An advertise request was made by a publisher, so we must notify all subscribers
	 * of the new topic
	 * @param topic - topic which a publisher wishes to advertise
	 * @throws RemoteException - thrown if there is an error in remote connection
	 */
	@Override
	public void addTopic(Topic topic) throws RemoteException {
		if(!topicSubscribers.containsKey(topic)) {
			topicSubscribers.put(topic, new ArrayList<>());
		}

		String msg = "A new topic has been advertised!\n" + topic;

		for(Subscriber s : onlineSubscribers) {
			s.printMessage(msg);
		}
	}

	/**
	 * A subscriber (new or existing) has logged in and must be added to our internal list
	 * of onlineSubscribers as well as allSubscribers (if new)
	 * @param s - the subscriber logging in
	 */
	@Override
	public void addSubscriber(Subscriber s) throws RemoteException, NotBoundException, MalformedURLException {
		Subscriber remoteS = (Subscriber)Naming.lookup("rmi://localhost:8080/" + s.getUsername());
		onlineSubscribers.add(remoteS);

		if(!allSubscribers.contains(remoteS.getUsername()))
			allSubscribers.add(remoteS.getUsername());
	}

	/**
	 * A subscriber is logging off, so remove them from internal list of onlineSubscribers
	 * @param s - the subscriber logging off
	 */
	@Override
	public void removeSubscriber(Subscriber s) throws RemoteException, NotBoundException, MalformedURLException {
		Subscriber remoteS = (Subscriber)Naming.lookup("rmi://localhost:8080/" + s.getUsername());
		onlineSubscribers.remove(remoteS);
	}

	/**
	 * The subscriber asks for a list of all the current topics  that have been advertised
	 * @return the list of all the topics
	 */
	@Override
	public List<Topic> listTopics() {
		return new ArrayList<>(topicSubscribers.keySet());
	}

	/**
	 * Retrieves a certain Topic object
	 * @param name - name of the Topic desired
	 * @return Topic with matching id
	 */
	private Topic returnTopic(String name) {
		for(Topic t : topicSubscribers.keySet())
			if(t.getName().equals(name))
				return t;
		return null;
	}

	/**
	 * A subscriber has just logged in and is checking to see if it missed any messages, send them
	 * all missed messages then remove them from the cache
	 * @param s - the subscriber making the request
	 * @return a formatted String containing all messages s missed while offline, returns special message if none
	 */
	@Override
	public String checkCache(Subscriber s) throws RemoteException {
		String cacheResult = "";
		String username = s.getUsername();

		if(messageCache.containsKey(username)) {
			int numMessages = messageCache.get(username).size();
			int currentMessage = 1;

			for(String msg : messageCache.get(username)) {
				cacheResult += "Message " + currentMessage + " of " + numMessages + ":\n";
				cacheResult += msg;
				currentMessage++;
			}

			messageCache.remove(username);
		} else {
			cacheResult = "You have no missed messages!\n";
		}

		return cacheResult;
	}

	/**
	 * determines if the subscriber that is currently logging in has already connected to the system
	 * @param s - subscriber trying to log in
	 * @return true if the agent has logged in before, false otherwise
	 */
	@Override
	public boolean inSubscribers(Subscriber s) throws RemoteException {
		for (String sub : allSubscribers){
			if (s.getUsername().equals(sub)){
				return true;
			}
		}
		return false;
	}

}
