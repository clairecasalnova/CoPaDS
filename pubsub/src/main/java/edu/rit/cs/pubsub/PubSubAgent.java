package edu.rit.cs.pubsub;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;


public class PubSubAgent  extends UnicastRemoteObject implements Publisher, Subscriber, Serializable {
    /**
     * the user name of the current publisher or subscriber
     */
    private String username;

    /**
     * the event manager that the pub/sub will connect to via RMI
     */
    private static RemoteEventManager eventManager;

    /***
     * sets up the RMI with the EventManager
     * takes the username that the user entered and determines if this agent already existed within the EventManager
     * constructs a new PubSubAgent using the username if the PSA has not already existed
     * @param username: the username of the PSA
     */
    public PubSubAgent(String username, Scanner scanner) throws RemoteException {

        super();

        this.username = username;

        try {
            Naming.rebind("rmi://localhost:8080/" + username, this);
            eventManager = (RemoteEventManager) Naming.lookup("rmi://localhost:8080/eventManager");

            if(eventManager.inSubscribers(this)) {
                getCachedMessages();
            }

            eventManager.addSubscriber(this);

            inputLoop(scanner);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @return username of this subscriber
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * Checks if obj is equal to this based on username
     * @param obj - other object
     * @return true if same username, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PubSubAgent) {
            return this.username.equals(((PubSubAgent) obj).username);
        }
        else{
            return false;
        }
    }

    /**
     * Uses username as the hash
     * @return a hashcode based on the username
     */
    @Override
    public int hashCode() {
        return username.hashCode();
    }

    /**
     * function to have a subscriber subscribe to a topic by name
     * calls the event manager to add the subscriber to the list of subscribers of that topic
     * @param topic the topic that the subscriber is being added to it
     */
    @Override
    public void subscribe(Topic topic) throws RemoteException {
        eventManager.addToTopic(topic,this);

    }

    /**
     * function to have a subscriber subscribe to topic by keywords
     * calls the event manager to add the subscriber to the list of subscribers topics that fall under that keyword
     * @param keyword the word that the subscriber wants to subscribe to
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    @Override
    public void subscribe(String keyword) throws RemoteException {
        eventManager.addToTopics(keyword, this);
    }

    /**
     * function that unsubscribes a user from a given topic
     * calls the event manager to remove the subscriber from the list
     * @param topic the topic that the user is unsubscribing from
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    @Override
    public void unsubscribe(Topic topic) throws RemoteException {
        eventManager.removeFromTopic(topic,this);

    }

    /**
     * function that unsubscribes a user from all of their subscribed topics
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    @Override
    public void unsubscribe() throws RemoteException {
        eventManager.removeFromTopics(this);
    }

    /**
     * displays a list of all the topics that the user is subscribed to
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    @Override
    public void listSubscribedTopics() throws RemoteException {
        System.out.println(eventManager.subscriberTopics(this));
    }

    /**
     * function called when a publisher wants to publish a new event to the subscribers
     * calls the event manager to notify each of the subscribers
     * @param event the new event to be published
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    @Override
    public void publish(Event event) throws RemoteException {
        eventManager.notifySubscribers(event);

    }

    /**
     * function called when a publisher wants to advertise a new topic to be subscribed to
     * calls event manager to notify all of the subscribers of the new topic
     * @param newTopic the new topic
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    @Override
    public void advertise(Topic newTopic) throws RemoteException {
        eventManager.addTopic(newTopic);

    }

    /**
     * gets the cached messages from the event manager when the subscriber goes offline to output when they come back
     * online
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    private void getCachedMessages() throws RemoteException {
        System.out.print(eventManager.checkCache(this));
    }

    /**
     * helper function to subscribe that determines if the user is subscribing via the topic name or keywords
     * @param sc the scanner to get input
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    private void subscribeAux(Scanner sc) throws RemoteException{
        System.out.println("Enter the command of the way you would like to subscribe:");
        System.out.println("\t1. Subscribe by topic name");
        System.out.println("\t2. Subscribe by key word");
        String i = sc.nextLine();
        int option = Integer.parseInt(i);
        switch(option){
            case 1:
                System.out.print("Please enter the topic name: ");
                String title = sc.nextLine();
                List<Topic> allTopics = eventManager.listTopics();

                boolean found = false;
                for (Topic t : allTopics){
                    if (t.getName().equals(title)){
                        found=true;
                        subscribe(t);
                    }
                }

                if (!found){
                    System.out.println("The topic you entered cannot be found please try again");
                } else{
                    System.out.println("You have been successfully subscribed!");
                }

                break;
            case 2:
                System.out.println("Please enter keywords you would like to subscribe to in a comma separated list");
                String keywords = sc.nextLine();
                String[] words = keywords.split(", |,");

                for (String w : words){
                    subscribe(w);
                }
                System.out.println("You have been successfully subscribed!");
                break;
            default:
                subscribeAux(sc);
                break;
        }

    }

    /**
     * helper function for unsubscriber
     * determines if this user is trying to unsubscribe from
     * @param sc the scanner to get input
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    private void unsubscribeAux(Scanner sc) throws RemoteException{
        System.out.println("Enter the command for how you want to unsubscribe:");
        System.out.println("\t1. Unsubscribe from specific topic");
        System.out.println("\t2. Unsubscribe from all topics");
        String i = sc.nextLine();
        int option = Integer.parseInt(i);
        switch(option){
            case 1:
                System.out.print("Enter the title of the topic you would like to unsubscribe from ");
                String s = sc.nextLine();
                List<Topic> allTopics = eventManager.listTopics();
                boolean found=false;

                for (Topic topic : allTopics) {
                    if (topic.getName().equals(s)) {
                        found=true;
                        unsubscribe(topic);
                    }
                }
                if (!found){
                    System.out.print("The topic you entered cannot be found please try again.");
                }
                else {
                    System.out.println("You have been successfully unsubscribed!");

                }
                break;
            case 2:
                unsubscribe();
                System.out.println("You have been successfully unsubscribed!");
                break;
            default:
                unsubscribeAux(sc);
                break;

        }
    }

    /**
     * helper function for publish
     * gets the information that the goes into the event and creates the event
     * @param sc the scanner to get the input
     * @throws RemoteException throws an exception if it cannot connect to the event manager
     */
    private void publishAux(Scanner sc) throws RemoteException{
        System.out.print("Please enter the topic (name) that this event is affiliated with: ");
        String topic = sc.nextLine();

        boolean found =false;
        List<Topic> allTopics = eventManager.listTopics();


        for (Topic t : allTopics){
            if (t.getName().equals(topic)){
                found = true;
                System.out.print("Please enter the title of this event: ");
                String title = sc.nextLine();
                System.out.print("Please enter the content of this event: ");
                String content = sc.nextLine();
                Event e = new Event(t,title,content);
                publish(e);
            }
        }

        if (!found) {
            System.out.println("The topic you entered cannot be found please try again.");
        }
        else{
            System.out.println("You have successfully published your topic");
        }

    }

    /**
     * helper function for advertise
     * gets the information about the topic and creates the topic
     * @param sc scanner to get input
     * @throws RemoteException throws an exception if the event manager cannot be reached
     */
    private void advertiseAux(Scanner sc) throws RemoteException{
        System.out.print("Please enter the name of this topic: ");
        String name = sc.nextLine();

        System.out.println("Please enter a comma separated list of keywords");
        String keywords = sc.nextLine();
        List<String> words = Arrays.asList(keywords.split(", |,"));
        Topic t = new Topic(name,words);
        advertise(t);

    }

    /**
     * lists all of the topics that have been advertised, special message if none
     * @throws RemoteException throws an exception if the event manager cannot be reached
     */
    private void showTopics() throws RemoteException {
        List<Topic> topics = eventManager.listTopics();

        if(topics.isEmpty())
            System.out.println("There are no existing topics.\n");
        else {
            for(Topic t : topics)
                System.out.println(t + "\n");
        }
    }

    /**
     * PubSubAgent constructor
     * has the user log in to the system and checks whether or not they have already been in the system
     * if they have already logged in that user is pulled from the event manager
     * if they have not yet logged in a new PubSubAgent is created
     */
    private static void login() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Welcome to RITea");
        System.out.print("Enter your username to login: ");
        String name = sc.nextLine();

        try {
            new PubSubAgent(name, sc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * function called when a user logs out of the system
     * event manager removes the subscriber from the list of online subscribers
     * @throws RemoteException throws an exception if the event manager cannot be reached
     */
    private void logout() throws RemoteException, MalformedURLException, NotBoundException {
        eventManager.removeSubscriber(this);
        Naming.unbind("rmi://localhost:8080/" + username);
    }

    /**
     * the input loop that outputs the message queue and the options for subscribe, publish, unsubscribe and advertise
     * @param sc - scanner using System.in
     * @throws RemoteException throws an exception if the event manager cannot be reached
     */
    private void inputLoop(Scanner sc) throws RemoteException, MalformedURLException, NotBoundException {

        while(true) {

            System.out.println("Please enter the number of the action to perform:");
            System.out.println("\t1. List all topics you are subscribed to");
            System.out.println("\t2. List all existing topics");
            System.out.println("\t3. Subscribe to a new topic");
            System.out.println("\t4. Unsubscribe from a topic");
            System.out.println("\t5. Publish a new event");
            System.out.println("\t6. Advertise a new topic");
            System.out.println("\t7. Log out");
            String command = sc.nextLine();
            int c = Integer.parseInt(command);

            switch (c){
                //subscribed topics
                case 1:
                    listSubscribedTopics();
                    break;

                //all existing topics
                case 2:
                    showTopics();
                    break;

                //subscribe to a new topic
                case 3:
                    subscribeAux(sc);
                    break;

                //unsubscribe from a topic
                case 4:
                    unsubscribeAux(sc);
                    break;

                //publish a new event
                case 5:
                    publishAux(sc);
                    break;

                //advertise new topic
                case 6:
                    advertiseAux(sc);
                    break;

                //logout
                case 7:
                    logout();
                    System.exit(0);
                    break;

                //repeat the input loop on failed input
                default:
                    break;
            }
        }
    }

    /**
     * Prints out a message that was sent by the Event Manager
     * @param msg - the message to display
     */
    public void printMessage(String msg) {
        System.out.println(msg + "\n");
    }

    /**
     * main function that runs the Pub/Sub Agent and all of the functions it can handle
     * sets up the scanner and begins the login and input loop process
     * @param args no command line args necessary
     */
    public static void main(String[] args) {
        login();
    }


}
