package edu.rit.cs.pubsub;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Subscriber extends Remote {

	/**
	 * @return username of this subscriber
	 */
	String getUsername() throws RemoteException;

	/**
	 * subscribe to a topic directly
	 * @param topic - topic to which subscriber wants to subscribe to
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void subscribe(Topic topic) throws RemoteException;

	/**
	 * subscribe to all topics with matching keyword
	 * @param keyword - keyword of topics that subscriber wants to subscribe to
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void subscribe(String keyword) throws RemoteException;

	/**
	 * unsubscribe from a topic directly
	 * @param topic - topic from which subscriber wants to unsubscribe
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void unsubscribe(Topic topic) throws RemoteException;

	/**
	 * unsubscribe from all subscribed topics
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void unsubscribe() throws RemoteException;

	/**
	 * shows the list of topics the subscriber is currently subscribed to
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void listSubscribedTopics() throws RemoteException;

	/**
	 * Prints out a message that was sent by the Event Manager
	 * @param msg - the message to display
	 * @throws RemoteException throws an exception if it cannot connect to the event manager
	 */
	void printMessage(String msg) throws RemoteException;
	
}
