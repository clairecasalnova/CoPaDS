package edu.rit.cs.pubsub;

import java.io.Serializable;
import java.util.List;

public class Topic implements Serializable {

	/**
	 * Used to keep track of the number of topics so an appropriate and unique ID can be assigned
	 */
	private static int numTopics = 0;

	/**
	 * Attributes of the Topic object
	 */
	private int id;
	private List<String> keywords;
	private String name;

	/**
	 * Constructor for the Topic object
	 * @param n - the name of the topic
	 * @param words - the list of keywords to be used for this topic
	 */
	public Topic(String n, List<String>words){
		id = numTopics++;
		keywords = words;
		name = n;

	}

	/**
	 * @return the id of this topic
	 */
	int getId() {
		return id;
	}

	/**
	 * @return the name of this topic
	 */
	String getName() {
		return name;
	}

	/**
	 * @return this list of keywords for this topic
	 */
	List<String> getKeywords() {
		return keywords;
	}

	/**
	 * Determines if obj is equal to this topic based on username
	 * @param obj - other object to test against
	 * @return true if equal, false if not
	 */
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Topic && name.equals(((Topic) obj).name);
	}

	/**
	 * Uses name as the hash
	 * @return - a hashcode based on the name
	 */
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	/**
	 * @return a formatted string describing the topic and its contents
	 */
	@Override
	public String toString() {
		String strTopic = "";

		strTopic += "Topic \"" + name + "\"\n";
		strTopic += "\tKeywords: ";

		//Format keywords so that no more than five show up on a line
		int formatCounter = 0;
		int totalCounter = 0;
		int numKeywords = keywords.size();
		for(String k : keywords) {
			strTopic += k;

			if(formatCounter < 4 && totalCounter < numKeywords - 1) {
				strTopic += ", ";
				formatCounter++;
			} else if(totalCounter < numKeywords - 1) {
				strTopic += ",\n\t\t\t  ";
				formatCounter = 0;
			}

			totalCounter++;
		}

		return strTopic;
	}


}
