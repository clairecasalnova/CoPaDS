package rmi_test;

//program for server application
import java.rmi.*;
import java.rmi.registry.*;

public class SearchServer {

    public static void main(String args[])
    {
        try
        {
            // Create an object of the interface implementation class
            Search obj = new SearchQuery();

            // rmiregistry within the server JVM with port number 8080
            LocateRegistry.createRegistry(8080);

            // Binds the remote object by the name testRMI
            Naming.rebind("rmi://localhost:8080/testRMI",obj);
        }
        catch(Exception ae)
        {
            System.out.println(ae);
        }
    }
}
