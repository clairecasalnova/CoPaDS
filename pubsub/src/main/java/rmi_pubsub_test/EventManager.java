/**package rmi_pubsub_test;

import edu.rit.cs.pubsub.RemoteEventManager;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class EventManager extends UnicastRemoteObject implements RemoteEventManager {

    private List<PubSubAgent> agents = new ArrayList<>();

    //Sets up this object as a remote object and then hands off to start()
    EventManager() throws RemoteException {
        super();

        // Creates an rmiregistry within the server JVM with port number 8080
        LocateRegistry.createRegistry(8080);

        // Binds this EventManager object by the name eventManager
        try {
            Naming.rebind("rmi://localhost:8080/eventManager", this);

            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Two test functions to make sure we can call these from PubSubAgent
    @Override
    public String showSubscribers(String topicName) throws RemoteException {
        return topicName + " and its topics...";
    }

    @Override
    public void addAgent(PubSubAgent psa) throws RemoteException {
        agents.add(psa);
        System.out.println("Added " + psa.getName());
        System.out.println(agents.get(0).listSubscribedTopics());
    }

    //This would be where we do stuff as in startService() in the real EventManager
    private void start() {

    }

    //This runs when you first run the class, then hands it over to constructor of this class
    public static void main(String[] args) {
        try {
            new EventManager();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}*/
