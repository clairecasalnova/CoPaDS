package rmi_pubsub_test;

import edu.rit.cs.pubsub.RemoteEventManager;

import java.io.Serializable;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class PubSubAgent implements Serializable {

    private RemoteEventManager eventManager;
    private String username;

    //Initializes the name (will use username in real scenario), gets the event manager, adds itself to
    // event managers list of agents and then hands off to start()
    PubSubAgent() throws RemoteException {
        super();

        username = "HERRO THERE";

        try {
            eventManager = (RemoteEventManager)Naming.lookup("rmi://localhost:8080/eventManager");
            //eventManager.addAgent(this);

            start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Some test functions that EventManager calls on actual instances of objects that it's able to store since
    // we added ourselves
    public String getName() {
        return username;
    }

    public String listSubscribedTopics() throws RemoteException {
        return "Here's a list of all the topics you have...";
    }

    //This function would be what inputLoop is in the real PubSubAgent
    private void start() throws RemoteException {
        //String response = eventManager.showSubscribers("Lou's Boo-hoos...");
        //System.out.println(response);
    }

    //Like EventManager, this runs first then hands it off to the constructor
    public static void main(String[] args) {
        try {
            new PubSubAgent();
        } catch(Exception e) {
            e.printStackTrace();
        }

    }
}
