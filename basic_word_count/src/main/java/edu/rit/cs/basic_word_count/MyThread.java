package edu.rit.cs.basic_word_count;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyThread implements Callable {

    private List<AmazonFineFoodReview> words;


    //constructor for the threads
    public MyThread(List<AmazonFineFoodReview> words) {
        this.words=words;

    }

    //runs the counting of the words in the threads
    @Override
    public Object call() throws Exception {
        List<String> result = new ArrayList<String>();
        for(AmazonFineFoodReview review : words) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                result.add(matcher.group().toLowerCase());
        }
        Map<String, Integer> wordcount = new HashMap<>();


        for(String word : result) {
            if(!wordcount.containsKey(word)) {
                wordcount.put(word, 1);
            } else{
                int init_value = wordcount.get(word);
                wordcount.replace(word, init_value, init_value+1);
            }
        }
        return wordcount;
    }
}
