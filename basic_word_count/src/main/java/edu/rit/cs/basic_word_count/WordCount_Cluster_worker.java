package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class WordCount_Cluster_worker {


    public static void main(String[] args) {
        //creates client sockets
        Socket socket = null;
        ObjectInputStream ois=null;
        ObjectOutputStream oos=null;
        List<AmazonFineFoodReview> sublist= null;

        //attempts to connect to the server socket
        try{
           socket = new Socket("129.21.67.206", 8080);
           ois= new ObjectInputStream(socket.getInputStream());
           oos= new ObjectOutputStream(socket.getOutputStream());

           sublist = (List<AmazonFineFoodReview>) ois.readObject();

           //partition again for threads
           int length=sublist.size();
           int partitions=4;
           int size= (int)Math.ceil(length/partitions);
           int start=0;
           int end=size;
           List[] group = new List[partitions];
           FutureTask[] tasks = new FutureTask[partitions];
           int index = 0;

           //divide the partition into more nodes
           for (int i = 0; i < partitions; i ++) {
               List<AmazonFineFoodReview> div = new ArrayList<>(sublist.subList(start, end));
               start+=size;
               end+=size;
               if (end>length){
                   end=length;
               }
               group[index]=div;
               index++;
           }

           /* Count words */
           Map<String, Integer> wordcount = new HashMap<>();



           //run threads to count the words
           for (int i=0;i<group.length;i++){
               Callable call= new MyThread(group[i]);
               tasks[i]= new FutureTask(call);
               Thread t = new Thread(tasks[i]);
               t.start();
               System.out.println("Thread starting");
               Map<String, Integer> result = new HashMap<>();
               result=(  Map<String, Integer> )tasks[i].get();
               for(String word : result.keySet()) {
                   if(!wordcount.containsKey(word)) {
                       wordcount.put(word, result.get(word));
                   }
                   else{
                       int init_value = wordcount.get(word);
                       wordcount.replace(word, init_value, init_value+result.get(word));
                   }
               }
           }

           //return the word count
           oos.writeObject(wordcount);

       }
       catch (Exception e){
           System.out.println(e.getMessage());
        }


    }
}
