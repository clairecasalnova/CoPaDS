package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;


public class WordCount_Cluster_master {

    public static final String AMAZON_FINE_FOOD_REVIEWS_file="amazon-fine-food-reviews/Reviews.csv";

    //reads the reviews in
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * prints out the words in a sorted order using a tree map
     * @param wordcount
     */
    public static void print_word_count( Map<String, Integer> wordcount){

        TreeMap<String,Integer> sorted = new TreeMap<>(wordcount);

        for(String word : sorted.keySet()){
            System.out.println(word + " : " + sorted.get(word));
        }
    }

    public static void main(String[] args){
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);
        System.out.println("Done reading");

        //creates the server socket
        ServerSocket ss= null;

        //determines the length of all of the partitions
        int length=allReviews.size();
        int partitions = 5;
        int size=(int) Math.ceil(length/partitions);

        int serverPort = 8080;

        //trys to connect to the sockets
        try{
             ss = new ServerSocket(serverPort);

            //partition the list
            int start=0;
            int end=size;
            List[] group = new List[partitions];
            int index = 0;

            //breaks the reviews into partitions
            for (int i = 0; i < partitions; i ++) {
                List<AmazonFineFoodReview> sublist = new ArrayList<>(allReviews.subList(start, end));
                start+=size;
                end+=size;
                if (end>length){
                    end=length;
                }
                group[index]=sublist;
                index++;
            }

            //connects to the sockets
            Socket[] sockets = new Socket[partitions];
            for (int i=0;i<partitions;i++) {
                sockets[i] = ss.accept();
                ObjectOutputStream oos = new ObjectOutputStream(sockets[i].getOutputStream());
                oos.writeObject(group[i]);
            }

            //times the merging
            MyTimer myTimer = new MyTimer("wordCount");
            myTimer.start_timer();

            Map<String, Integer> wordcount = new HashMap<>();
            ObjectInputStream ois= null;


            //merges all of the sorts back together
            for (int i=0;i<sockets.length;i++) {
                ois = new ObjectInputStream(sockets[i].getInputStream());
                Map<String, Integer> result = (Map<String, Integer>) ois.readObject();
                for (String word : result.keySet()) {
                    if (!wordcount.containsKey(word)) {
                        wordcount.put(word, result.get(word));
                    } else {
                        int init_value = wordcount.get(word);
                        wordcount.replace(word, init_value, init_value + result.get(word));
                    }
                }
            }

            myTimer.stop_timer();


            //prints the words in order and the time elapsed
            print_word_count(wordcount);
            myTimer.print_elapsed_time();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }


    }
}
