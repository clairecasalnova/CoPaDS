package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WordCount_Threads {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="amazon-fine-food-reviews/Reviews.csv";

    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }


    public static void print_word_count( Map<String, Integer> wordcount){

        TreeMap<String,Integer> sorted = new TreeMap<>(wordcount);

        for(String word : sorted.keySet()){
            System.out.println(word + " : " + sorted.get(word));
        }
    }

    public static void main(String[] args) throws Exception{
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        int length=allReviews.size();
        int partitions = 4;
        int size=(int) Math.ceil(length/partitions);

        List[] group = new List[partitions];
        FutureTask[] tasks = new FutureTask[partitions];
        int index = 0;

        int start=0;
        int end=size;

        //splits the reviews into partitions
        for (int i = 0; i < partitions; i ++) {
            List<AmazonFineFoodReview> sublist = allReviews.subList(start, end);
            start+=size;
            end+=size;
            if (end>length){
                end=length;
            }
            group[index]=sublist;
            index++;
        }


        /* For debug purpose */
//        for(AmazonFineFoodReview review : allReviews){
//            System.out.println(review.get_Text());
//        }

        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();
        /* Tokenize words */

//        /* Count words */
        Map<String, Integer> wordcount = new HashMap<>();


        //starts each of the threads and merges the final results together

        for (int i=0;i<group.length;i++){
            Callable call= new MyThread(group[i]);
            tasks[i]= new FutureTask(call);
            Thread t = new Thread(tasks[i]);
            t.start();
            Map<String, Integer> result = new HashMap<>();
            result=(  Map<String, Integer> )tasks[i].get();
            for(String word : result.keySet()) {
                if(!wordcount.containsKey(word)) {
                    wordcount.put(word, result.get(word));
                }
                else{
                    int init_value = wordcount.get(word);
                    wordcount.replace(word, init_value, init_value+result.get(word));
                }
            }
        }

        myTimer.stop_timer();

        print_word_count(wordcount);

        myTimer.print_elapsed_time();
    }

}
